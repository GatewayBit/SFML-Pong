#ifndef LOGIC_H
#define LOGIC_H
#include "SFML/Graphics.hpp"
#include "properties.h"

bool playerScored(sf::Vector2f *bPos);
bool leftPlayerScored(sf::Vector2f *bPos);

#endif
