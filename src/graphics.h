#ifndef GRAPHICS_H
#define GRAPHICS_H
#include "SFML/Graphics.hpp"
#include "properties.h"

void initGraphicTexture();
void createLeftPaddle(sf::Sprite *playerPaddle);
void createRightPaddle(sf::Sprite *playerPaddle);
void createBall(sf::Sprite *ballSprite);

#endif
