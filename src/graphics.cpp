#include "graphics.h"
#define PADDLE_X_SIZE 10
#define PADDLE_Y_SIZE 100

sf::Texture graphicTexture;

void initGraphicTexture() {
    graphicTexture.create(1,1);

    const int pixelArraySize = PADDLE_X_SIZE * PADDLE_Y_SIZE * 4;
    sf::Uint8* pixels = new sf::Uint8[pixelArraySize];

    // Can be used to set each pixel data color individually.
   // const sf::Uint8 r = 255;
   // const sf::Uint8 g = 255;
   // const sf::Uint8 b = 255;
   // const sf::Uint8 a = 255;
   // for (int i = 0; i < pixelArraySize; i+= 4) {
   //     pixels[i] = r;
   //     pixels[i+1] = g;
   //     pixels[i+2] = b;
   //     pixels[i+3] = a;
   // }

    for (int i = 0; i < pixelArraySize; i++) {
        pixels[i] = 255;
    }
    graphicTexture.update(pixels);
}

void createLeftPaddle(sf::Sprite *playerPaddle) {
    playerPaddle->setTexture(graphicTexture);
    playerPaddle->setTextureRect(sf::IntRect(0, 0, PADDLE_X_SIZE, PADDLE_Y_SIZE));
    playerPaddle->setColor(sf::Color(4, 82, 209, 255));
    playerPaddle->setOrigin(0, PADDLE_Y_SIZE / 2);
    playerPaddle->setPosition(LEFT_PLAYER_X_POS, HEIGHT / 2);
}

void createRightPaddle(sf::Sprite *playerPaddle) {
    playerPaddle->setTexture(graphicTexture);
    playerPaddle->setTextureRect(sf::IntRect(0, 0, PADDLE_X_SIZE, PADDLE_Y_SIZE));
    playerPaddle->setColor(sf::Color(196, 7, 7, 255));
    playerPaddle->setOrigin(0, PADDLE_Y_SIZE / 2);
    playerPaddle->setPosition(RIGHT_PLAYER_X_POS, HEIGHT / 2);
}

void createBall(sf::Sprite *ballSprite) {
    ballSprite->setTexture(graphicTexture);
    ballSprite->setTextureRect(sf::IntRect(0, 0, BALL_X_SIZE, BALL_Y_SIZE));
    ballSprite->setColor(sf::Color(255, 255, 255, 255));
    ballSprite->setOrigin(BALL_X_SIZE / 2, BALL_Y_SIZE / 2);
    ballSprite->setPosition(WIDTH / 2, HEIGHT / 2);
}
