#include "ball.h"
#include "properties.h"

void ballReset(sf::Vector2f *bPos) {
    bPos->x = WIDTH / 2;
    bPos->y = HEIGHT / 2;
}

bool ballBounce(sf::Vector2f *bPos) {
    return bPos->y > HEIGHT || bPos->y < 0 ? true : false;
}
