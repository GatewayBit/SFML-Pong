#ifndef BALL_H
#define BALL_H
#include "SFML/Graphics.hpp"

bool leftPlayerScored(sf::Vector2f *bPos);
bool ballBounce(sf::Vector2f *bPos);
bool ballCollidesWithPaddle();
bool ballHitsLeftPaddle();
void ballReset(sf::Vector2f *bPos);
void ballScored(sf::Vector2f &bPos);

#endif
