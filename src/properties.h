#ifndef PROPERTIES_H
#define PROPERTIES_H
#define SCREEN_OFFSET 15

const unsigned short int WIDTH = 800;
const unsigned short int HEIGHT = 600;

const unsigned short int BALL_X_SIZE = 15;
const unsigned short int BALL_Y_SIZE = 15;

const float LEFT_PLAYER_X_POS = SCREEN_OFFSET;
const float RIGHT_PLAYER_X_POS = WIDTH - SCREEN_OFFSET * 2;

#endif
