#include <iostream>
#include <sstream>
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "ball.h"
#include "properties.h"
#include "logic.h"
#include "graphics.h"

unsigned short int leftScore = 0;
unsigned short int rightScore = 0;

float ballMoveSpeed = 2.5f;

sf::Vector2f moveBall(sf::Sprite b);

template <typename T>
std::string numberToString ( T Number );

sf::Sound wallHitSound;
sf::Sound padHitSound;
sf::Sound ballScoreSound;

sf::FloatRect leftPadBoundingBox;
sf::FloatRect rightPadBoundingBox;
sf::FloatRect ballBoundingBox;

std::string leftScoreStr;
std::string rightScoreStr;
std::ostringstream convert;

std::string outputText = "SCORE " + leftScoreStr + " - " + rightScoreStr;

int main() {
    const unsigned short int PADDLE_TOP = 50;

    const float MOVE_SPEED = 5.0f;


    sf::SoundBuffer padHitBuffer;
    if (!padHitBuffer.loadFromFile("audio/padHit.wav")) {
        return -1;
    }
    padHitSound.setBuffer(padHitBuffer);

    sf::SoundBuffer ballScoreBuffer;
    if (!ballScoreBuffer.loadFromFile("audio/ballScore.wav")) {
        return -1;
    }
    ballScoreSound.setBuffer(ballScoreBuffer);

    sf::SoundBuffer wallHitBuffer;
    if (!wallHitBuffer.loadFromFile("audio/wallHit.wav")) {
        return -1;
    }
    wallHitSound.setBuffer(wallHitBuffer);


    initGraphicTexture();

    sf::Sprite leftPaddle;
    sf::Sprite rightPaddle;
    sf::Sprite ball;

    createLeftPaddle(&leftPaddle);
    createRightPaddle(&rightPaddle);
    createBall(&ball);

    sf::Vector2f leftPadPos = leftPaddle.getPosition();
    sf::Vector2f rightPadPos = rightPaddle.getPosition();

    sf::Font bitter;
    bitter.loadFromFile("Bitter-Regular.ttf");

    sf::Text text;
    text.setFont(bitter);

    leftScoreStr = numberToString(leftScore);
    rightScoreStr = numberToString(rightScore);

    std::cout << leftScore << " - " << rightScore << '\n';
    std::cout << leftScoreStr << " - " << rightScoreStr << '\n';

    outputText = "SCORE " + leftScoreStr + " - " + rightScoreStr;

    text.setString(outputText);
    text.setCharacterSize(24);
    text.setFillColor(sf::Color(50, 255, 255, 255));
    text.setPosition((WIDTH / 2) - 75, 15);

    sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "Pong");

    window.setFramerateLimit(120);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            if (event.type == sf::Event::Resized) {
                sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
                window.setView(sf::View(visibleArea));
            }
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
        {
            leftPadPos.y -= MOVE_SPEED;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
        {
            leftPadPos.y += MOVE_SPEED;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            rightPadPos.y -= MOVE_SPEED;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            rightPadPos.y += MOVE_SPEED;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            window.close();
        }

        if (leftPadPos.y - PADDLE_TOP <= 0)
        {
            leftPadPos.y = PADDLE_TOP;
        }
        else if (leftPadPos.y + PADDLE_TOP >= HEIGHT)
        {
            leftPadPos.y = HEIGHT - PADDLE_TOP;
        }

        if (rightPadPos.y - PADDLE_TOP <= 0)
        {
            rightPadPos.y = PADDLE_TOP;
        }
        else if (rightPadPos.y + PADDLE_TOP >= HEIGHT)
        {
            rightPadPos.y = HEIGHT - PADDLE_TOP;
        }


        leftPadBoundingBox = leftPaddle.getGlobalBounds();
        rightPadBoundingBox = rightPaddle.getGlobalBounds();
        ballBoundingBox = ball.getGlobalBounds();

        leftPaddle.setPosition(leftPadPos.x, leftPadPos.y);
        rightPaddle.setPosition(rightPadPos.x, rightPadPos.y);

        ball.setPosition(moveBall(ball));

        text.setString(outputText);

        window.clear();
        window.draw(leftPaddle);
        window.draw(rightPaddle);
        window.draw(ball);
        window.draw(text);
        window.display();
    }

    return 0;
}


float xDir = 1;
float yDir = 1;

sf::Vector2f moveBall(sf::Sprite b) {
    sf::Vector2f bPos = b.getPosition();

    ballScored(bPos);
    
    if (ballBounce(&bPos)) {
        yDir *= -1;
        wallHitSound.play();
    }

    // Fixes bug where ball is stuck inside bounding box.
    if (ballCollidesWithPaddle()) {
	    if (ballHitsLeftPaddle()) {
		    xDir = 1;
	    } else {
		    xDir = -1;
	    }
        padHitSound.play();
	    ballMoveSpeed += 0.25f;
    }


    bPos.x += ballMoveSpeed * xDir;
    bPos.y += ballMoveSpeed * yDir;

    return bPos;
}

bool ballCollidesWithPaddle() {
    return ballBoundingBox.intersects(leftPadBoundingBox) || ballBoundingBox.intersects(rightPadBoundingBox) ? true : false;
}

bool ballHitsLeftPaddle() {
    return ballBoundingBox.intersects(leftPadBoundingBox) ? true : false;
}

void ballScored(sf::Vector2f &bPos) {
    // Someone got a point...but who?
    if (playerScored(&bPos)) {
        // Ball hit the X edge of the screen. Reset from middle.
        if (leftPlayerScored(&bPos)) {
            // Left player scores!
            std::cout << "LEFT SCORES!" << '\n';
            leftScore += 1;
            leftScoreStr = numberToString(leftScore);
        } else {
            // Right player scores!
            std::cout << "RIGHT SCORES!" << '\n';
            rightScore += 1;
            rightScoreStr = numberToString(rightScore);
        }
        ballScoreSound.play();
        // Update the score.
        outputText = "SCORE " + leftScoreStr + " - " + rightScoreStr;

	    std::cout << "bPos.x: " << bPos.x << "\nbPos.y: " << bPos.y << "\n\n";

        ballReset(&bPos);
        ballMoveSpeed = 2.5F;
        xDir *= -1;
    }
}



template <typename T>
std::string numberToString (T number) {
    std::ostringstream ss;
    ss << number;
    return ss.str();
}
